.PHONY: sdbforth-clean

sdbforth_c_src := $(wildcard src/*.c)
sdbforth_c_obj := $(sdbforth_c_src:src/%.c=obj/%.o)
sdbforth_amd64_src := $(wildcard src/amd64/*.s)
sdbforth_amd64_obj := $(sdbforth_amd64_src:src/%.s=obj/%.o)
all_objs += $(sdbforth_c_obj) $(sdbforth_amd64_obj)
sdbforth_libs := -ldl

all: bin/sdbforth

$(sdbforth_c_obj): obj/%.o: src/%.c obj/%.d | obj/
	$(CC) -c -MMD -MP -fpic -g $(CPPFLAGS) $(CFLAGS) $(TARGET_ARCH) $< -o $@

$(sdbforth_amd64_obj): obj/amd64/%.o: src/amd64/%.s | obj/amd64/
	$(CC) -c -g $< -o $@

bin/sdbforth: $(sdbforth_c_obj) $(sdbforth_amd64_obj) | bin/
	$(CC) -g $(CFLAGS) $(TARGET_ARCH) $^ $(LDFLAGS) $(sdbforth_libs) $(LIBS) -o $@

clean: sdbforth-clean

sdbforth-clean:
	$(RM) bin/sdbforth
