This is a FORTH environment, based on jonesforth (sources included).

It is designed to run on an AMD64 / x86_64 cpu, running linux as an operating system.

A C runtime library is required.
Some constants are taken from glibc: if you are using a different C library you may need to change these.

This program does not support being built as a statically-linked binary.

Runtime library loading is required.
It is expected that `-ldl` is the correct switch to link the runtime dynamic loader.
If this is not the case you will need to modify `sdbforth.mk`.

The `SDL_demo.ks` script requires SDL2 and SDL2_ttf to run.
Please ensure these are installed in appropriate locations.

You'll need the standard GNU build-tools (or equivalent): make, binutils, and GCC.

Running `make` will produce bin/sdbforth.
You can then run the example programs with the commands `bin/sdbforth friendly.ks --` or `bin/sdbforth interpreter.ks SDL_demo.ks --`
Note that the `--` is important.

Please read the LICENSE file before copying this project.

Note that this archive also contains the original jonesforth source (in directory `jonesforth/`) which this software is based on.
Jonesforth has a public-domain declaration.

Also included in this archive is the Ubuntu font family.
They are supplied under their own license.


INVOKING SDBFORTH

sdbforth [ SCRIPT... -- ] args
Zero or more script files may be present before the `--` token. These will be interpreted immediately, and will not appear as arguments to the running program.
The `--` marks the end of the script files. It will be replaced with the interpreter's name and used as the 0th argument in the running program.
Any arguments after `--` are treated as normal arguments, and are passed straight to the program.
The builtin variables ARGC and ARGV get the argument count and list, respectively.

Scripts may also include other scripts with the INCLUDE and INCLUDE" keywords.
INCLUDE loads a file from the zero-terminated path on top of the stack.
INCLUDE" reads a path up to the next ".

Both variants then interpret the given file in place.
