/*
	We are running in 64-bit mode, but should prefer to use 32-bit arithmetic when possible
	as the instructions are smaller, and we will rarely require the full 64-bit range of values.
	This obviously doesn't apply to things like pointers, which must be the full 8-byte values.
	We're using gas (that's the GNU Assembler) because I already have it installed, and it's what I know.
	The only assembly we'll be writing is to get our Forth environment set up.
	After that, if we need to write any more machine code, we can do it with Forth.

	We're actually going to set up two Forth environments:
	The first will be used to bootstrap the second, but will be missing many useful features.
	The second environment will be written in Forth, and will replace the bootstrap system.

	This is based laregly on JonesForth, but adapted to be 64-bit code.
	We aren't attempting to conform to the ANS Forth standard.

	This environment is for Linux on AMD64.
	There is a small amount of C code that performs initialisation before calling 'begin'.
	We can therefore use any capabilities of the standard C library.

	The standard calling convention (for UNIX-like systems, anyway) requires us to store %rsi before calling an external routine.

	NEXT loads the codeword of the next word and then executes it.
	The codeword acts as the interpreter for the word.
	It's usually a pointer to another word like DOCOL that can run the word.
	For words defined by machine code, its just a pointer to the first instruction.
	We use the LODS instruction to get the target address.
	LODS is the LOaD String instruction.
	It reads the memory from rSI into rAX, then increments (or decrements if the direction flag is set) rSI
	We're using the 8-byte variant (lodsq) as we need to read pointers.
*/
	.macro NEXT
	lodsq
	jmp *(%rax)
	.endm

/*
	Push / pop the return stack
	Note that we are using %rbp to store the top of the return stack (hence RSP - Return Stack Pointer).
	Conventionally it would be used as the frame pointer.
*/
	.macro PUSHRSP reg
	leaq -8(%rbp), %rbp
	movq \reg, (%rbp)
	.endm

	.macro POPRSP reg
	movq (%rbp), \reg
	leaq 8(%rbp), %rbp
	.endm

/* We'll use the normal push and pop instructions for the data stack. */

/* DOCOL (DO COLon) interprets words. */
	.text
	.align 8
DOCOL:
	PUSHRSP %rsi    /* Push %rsi (the next word) onto the return stack - we'll jump here when we're done. */
	addq $8, %rax   /* %rax contains the address we just jumped from - 8 bytes later is the definition. */
	movq %rax, %rsi /* We're expecting to load a codeword, so make it the next word. */
	NEXT            /* Jump to the word. */

/*
	Begin is called from main after allocating space for the data stack.
	It marks the start of stack, sets up the data & return stacks, and populates a few variables.
*/
	.text
	.globl begin
begin:
	movq %rsp, var_S0(%rip) /* S0 contains the address of the start of the stack */
	leaq rstack_top(%rip), %rbp

/*
	Set up some variables.
	ARGC & ARGV are passed in as arguments, and correspond to (ie. are exactly) the arguments passed to main()
	HERE gets the value of data_start, which will have been initialised in main()
	DATA_START gets the same value
	DATA_END gets data_start + data_end, it marks the end of our allocated data region.
*/
	movq %rdi, var_ARGC(%rip)
	movq %rsi, var_ARGV(%rip)
	movq data_start(%rip), %rax
	movq %rax, var_HERE(%rip)
	movq %rax, var_DATA_START(%rip)
	movq data_size(%rip), %rbx
	addq %rbx, %rax
	movq %rax, var_DATA_END(%rip)

/* To start the interpreter we make cold_start the next instruction, and then jump to it. */
	leaq cold_start(%rip), %rsi
	NEXT

/* cold_start just calls QUIT, which will run the interpreter. QUIT will never return. */
	.section .rodata
	.align 8
cold_start:
	.quad QUIT

/*
	Our dictionary structure looks like this:

	LINK ADDRESS | FLAGS  | NAME LENGTH | NAME + PADDING        | CODEWORD | INSTRUCTIONS / DATA
	8 bytes      | 1 byte | 1 byte      | NAME_LENGTH (aligned) | 8 bytes  | Unspecified
*/

/*
	Flags for entries in the dictionary.
*/
	.set F_IMMED, 0x01  /* Indicates that a word should be executed immediately, not compiled. */
	.set F_HIDDEN, 0x02 /* Indicates that a word should not be matched by FIND. */

/*
	link is used to build the dictionary at compile time.
	The first entry (which happens to be DROP) will have link value 0.
	This will be updated by the assembler whenever a word is defined.
*/
	.set link, 0

/*
	The defword macro defines a Forth word.
	Following should be a series of .quad instructions specifying the words to execute.
	Words should end with .quad EXIT
*/
	.macro defword name, namelen, flags=0, label
	.section .rodata
	.align 8
name_\label:
	.quad link
	.set link, name_\label
	.byte \flags
	.byte \namelen
	.ascii "\name"
	.align 8
\label:
	.quad DOCOL
	.endm

/*
	The defcode macro defines a builtin codeword.
	Following should be the instructions required to execute the codeword, usually followed by NEXT.
*/
	.macro defcode name, namelen, flags=0, label
	.section .rodata
	.align 8
name_\label:
	.quad link
	.set link, name_\label
	.byte \flags
	.byte \namelen
	.ascii "\name"
	.align 8
\label:
	.quad code_\label
	.text
code_\label:
	.endm

/* Common stack operations. */
	defcode "DROP", 4, , DROP
	pop %rax
	NEXT

	defcode "SWAP", 4, , SWAP
	pop %rax
	pop %rbx
	push %rax
	push %rbx
	NEXT

	defcode "DUP", 3, , DUP
	movq (%rsp), %rax
	push %rax
	NEXT

	defcode "OVER", 4, , OVER
	movq 8(%rsp), %rax
	push %rax
	NEXT

	defcode "ROT", 3, , ROT
	pop %rax
	pop %rbx
	pop %rcx
	push %rbx
	push %rax
	push %rcx
	NEXT

	defcode "-ROT", 4, , NROT
	pop %rax
	pop %rbx
	pop %rcx
	push %rax
	push %rcx
	push %rbx
	NEXT

	defcode "2DROP", 5, , TWODROP
	pop %rax
	pop %rax
	NEXT

	defcode "2DUP", 4, , TWODUP
	movq (%rsp), %rax
	movq 8(%rsp), %rbx
	push %rbx
	push %rax
	NEXT

	defcode "2SWAP", 5, , TWOSWAP
	pop %rax
	pop %rbx
	pop %rcx
	pop %rdx
	push %rbx
	push %rax
	push %rdx
	push %rcx
	NEXT

/* ?DUP duplicates the top of stack only if it is non-zero. */
	defcode "?DUP", 4, , QDUP
	movq (%rsp), %rax
	test %rax, %rax
	jz 1f
	push %rax
1:
	NEXT

/* Optimised routines for adding/subtracting 1 and 8, because these are very common operations (8 is the pointer size on a amd64, and we end up using it a lot). */
	defcode "1+", 2, , INCR
	incq (%rsp)
	NEXT

	defcode "1-", 2, , DECR
	decq (%rsp)
	NEXT

	defcode "8+", 2, , INCR8
	addq $8, (%rsp)
	NEXT

	defcode "8-", 2, , DECR8
	subq $8, (%rsp)
	NEXT

/* Arithmetic operations. */
	defcode "+", 1, , ADD
	pop %rax
	addq %rax, (%rsp)
	NEXT

	defcode "-", 1, , SUB
	pop %rax
	subq %rax, (%rsp)
	NEXT

	defcode "*", 1, , MUL
	pop %rax
	pop %rbx
	imulq %rbx, %rax
	push %rax
	NEXT

/* Leaves the quotient on top of the stack, followed by the remainder. */
	defcode "/MOD", 4, , DIVMOD
	xor %rdx, %rdx
	pop %rbx
	pop %rax
	idivq %rbx
	push %rdx
	push %rax
	NEXT

/* macro to assist creation of comparison operations */
	.macro cmp_op name, namelen, label, op
	defcode "\name", \namelen, 0, \label
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	\op %al
	decb %al
	movsbq %al, %rax
	push %rax
	NEXT
	.endm

/* Comparison operations. */
	cmp_op "=",  1, EQU,  setne
	cmp_op "<>", 2, NEQU, sete
	cmp_op "<",  1, LT,   setge
	cmp_op ">",  1, GT,   setle
	cmp_op "<=", 2, LE,   setg
	cmp_op ">=", 2, GE,   setl

/* macro to assist creation of compare-to-zero operations */
	.macro zcmp_op name, namelen, label, op
	defcode "\name", \namelen, , \label
	pop %rax
	test %rax, %rax
	\op %al
	decb %al
	movsbq %al, %rax
	push %rax
	NEXT
	.endm

/* Compare to zero operations. */
	zcmp_op "0=",  2, ZEQU,  setnz
	zcmp_op "0<>", 3, ZNEQU, setz
	zcmp_op "0<",  2, ZLT,   setge
	zcmp_op "0>",  2, ZGT,   setle
	zcmp_op "0<=", 3, ZLE,   setg
	zcmp_op "0>=", 3, ZGE,   setl

/*
	Boolean logic / bitwise operations.
	By using -1 to represent TRUE, these are exactly the same instructions.
*/
	defcode "AND", 3, , AND
	pop %rax
	andq %rax, (%rsp)
	NEXT

	defcode "OR", 2, , OR
	pop %rax
	orq %rax, (%rsp)
	NEXT

	defcode "XOR", 3, , XOR
	pop %rax
	xorq %rax, (%rsp)
	NEXT

	defcode "NOT", 3, , NOT
	notq (%rsp)
	NEXT

/* INVERT is a synonym for NOT */
	defcode "INVERT", 6, , INVERT
	jmp code_NOT

/* Return from a word. */
	defcode "EXIT", 4, , EXIT
	POPRSP %rsi
	NEXT

/* Don't interpret the following word, just load its value. */
	defcode "LIT", 3, , LIT
	lodsq
	push %rax
	NEXT

/* Store / Fetch */
	defcode "!", 1, , STORE
	pop %rbx
	pop %rax
	movq %rax, (%rbx)
	NEXT

	defcode "@", 1, , FETCH
	pop %rbx
	movq (%rbx), %rax
	push %rax
	NEXT

	defcode "+!", 2, , ADDSTORE
	pop %rbx
	pop %rax
	addq %rax, (%rbx)
	NEXT

	defcode "-!", 2, , SUBSTORE
	pop %rbx
	pop %rax
	subq %rax, (%rbx)
	NEXT

	defcode "C!", 2, , STOREBYTE
	pop %rbx
	pop %rax
	movb %al, (%rbx)
	NEXT

	defcode "C@", 2, , FETCHBYTE
	pop %rbx
	xor %eax, %eax
	movb (%rbx), %al
	push %rax
	NEXT

/* C@C! ( s d -- s+1 d+1 )
Copies a byte from s and stores it in d.
Increments s & d.
*/
	defcode "C@C!", 4, , CCOPY
	movq 8(%rsp), %rbx
	movb (%rbx), %al
	pop %rdi
	stosb
	push %rdi
	incq 8(%rsp)
	NEXT

/* CMOVE ( s d n -- )
Copy n bytes from s into d.
*/
	defcode "CMOVE", 5, , CMOVE
	mov %rsi, %rdx
	pop %rcx
	pop %rdi
	pop %rsi

	cmpq %rdi, %rsi
	jns 1f
/* source > dest - go backwards */
	std
	add %rcx, %rdi
	dec %rdi
	add %rcx, %rsi
	dec %rsi
	rep movsb
	cld
	jmp 2f
1:
/* source < dest - go forwards */
	rep movsb
2:
	mov %rdx, %rsi
	NEXT

/* Macro to define a variable */
	.macro defvar name, namelen, flags=0, label, initial=0
	defcode \name, \namelen, \flags, \label
	leaq var_\name(%rip), %rax
	push %rax
	NEXT
	.data
	.align 8
var_\name:
	.quad \initial
	.endm

/* Variables */
	defvar "STATE", 5, , STATE                  /* Compiler state: 1 = compile, 0 = interpret */
	defvar "HERE", 4, , HERE                    /* Address to store data into */
	defvar "LATEST", 6, , LATEST, name_SYSCALL0 /* LATEST will store the last word added in the dictionary, initialise it to the last word defined in this file (SYSCALL0) */
	defvar "S0", 2, , SZ                        /* Stores the end of the data stack. */
	defvar "BASE", 4, , BASE, 10                /* Base for number printing / reading */
	defvar "DATA_START", 10, , DATA_START       /* Beginning of data segment */
	defvar "DATA_END", 8, , DATA_END            /* End of data segment */
	defvar "ARGC", 4, , ARGC                    /* Number of program arguments (should be at least one - the name of the program). */
	defvar "ARGV", 4, , ARGV                    /* Program arguments. This has the same structure as it would in C's main function: ARGC pointers to null-terminated strings, followed by a NULL pointer. */

/* Macro to define a constant */
	.macro defconst name, namelen, flags=0, label, value
	defcode \name, \namelen, \flags, \label
	pushq $\value
	NEXT
	.endm

/* Conceptually R0 and DOCOL are constants, but the macro only works for immediates, and DOCOL and R0 need memory addresses */

/* R0 stores the end of the return stack. */
	defcode "R0", 2, , RZ
	leaq rstack_top(%rip), %rax
	push %rax
	NEXT

/* DOCOL stores the location of the actual DOCOL body */
	defcode "DOCOL", 5, , __DOCOL
	leaq DOCOL(%rip), %rax
	push %rax
	NEXT

/* Expose F_IMMED and F_HIDDEN as Forth words */
	defconst "F_IMMED",    7, , __F_IMMED,  F_IMMED
	defconst "F_HIDDEN",   8, , __F_HIDDEN, F_HIDDEN

/*
	Useful system calls.
	It's probably best to use the libc wrappers when possible.
	Importantly, exit() should always be invoked by the libc wraper, so that the C library can do it's cleanup (calling it directly is roughly equivalent to calling _Exit() from a C program).
*/
	defconst "SYS_READ",   8, , SYS_READ,   0
	defconst "SYS_WRITE",  9, , SYS_WRITE,  1
	defconst "SYS_OPEN",   8, , SYS_OPEN,   2
	defconst "SYS_CLOSE",  9, , SYS_CLOSE,  3

/* Flags for SYS_OPEN */
	defconst "O_RDONLY"    8, , __O_RDONLY,   0
	defconst "O_WRONLY",   8, , __O_WRONLY,   1
	defconst "O_RDWR",     6, , __O_RDWR,     2
	defconst "O_CREAT",    7, , __O_CREAT,    0100
	defconst "O_EXCL",     6, , __O_EXCL,     0200
	defconst "O_TRUNC",    7, , __O_TRUN,     01000
	defconst "O_APPEND",   8, , __O_APPEND,   02000
	defconst "O_NONBLOCK", 10,, __O_NONBLOCK, 04000

/* Access the return stack */
	defcode ">R", 2, , TOR
	pop %rax
	PUSHRSP %rax
	NEXT

	defcode "R>", 2, , FROMR
	POPRSP %rax
	push %rax
	NEXT

	defcode "RSP@", 4, , RSPFETCH
	push %rbp
	NEXT

	defcode "RSP!", 4, , RSPSTORE
	pop %rbp
	NEXT

	defcode "RDROP", 5, , RDROP
	addq $8, %rbp
	NEXT

/* Get / set the data stack address */
	defcode "DSP@", 4, , DSPFETCH
	mov %rsp, %rax
	push %rax
	NEXT

	defcode "DSP!", 4, , DSPSTORE
	pop %rsp
	NEXT

/* Read a byte from standard input. Calls a helper routine written in C. */
	defcode "KEY", 3, , KEY
	movq %rsi, %r13
	call _KEY
	movq %r13, %rsi
	movzbq %al, %rax
	push %rax
	NEXT
_KEY:
	mov $0xf, %r14
	and %rsp, %r14
	sub %r14, %rsp
	call real_KEY@PLT
	addq %r14, %rsp
	ret

/* Write a byte to standard output. */
	defcode "EMIT", 4, , EMIT
	pop %rax
	call _EMIT
	NEXT
_EMIT:
	mov %rsi, %r13
	mov $1, %edi
	leaq emit_scratch(%rip), %rsi
	movb %al, (%rsi)
	mov $1, %rdx
	mov $1, %rax
	syscall
	mov %r13, %rsi
	ret

	.data
emit_scratch:
	.space 1

/*
	Read a word from standard input.
	Skips leading whitespace & comments.
	Words begin with a non-space, non-control character and don't contain these characters.
	Comments begin with \ and extend to the end of the line.
	Calling _WORD directly leaves the length in %rcx and the first character's address in %rdi.
	Otherwise the stack effects are ( -- addr length )
*/
	defcode "WORD", 4, , WORD
	call _WORD
	push %rdi
	push %rcx
	NEXT
_WORD:
	mov %rsi, %r13
1:
	call _KEY
	cmpb $'\\', %al
	je 4f
	cmpb $' ', %al
	jbe 1b

	leaq word_buffer(%rip), %rbx
	xor %r14d, %r14d
2:
	cmp $254, %r14
	je 3f
	movq %rbx, %rdi
	stosb
	movq %rdi, %rbx
	inc %r14d
3:
	call _KEY
	cmpb $' ', %al
	ja 2b

	xor %eax, %eax
	stosb

	leaq word_buffer(%rip), %rdi
	subq %rdi, %rbx
	movq %rbx, %rcx
	mov %r13, %rsi
	ret
4:
	call _KEY
	cmpb $'\n', %al
	jne 4b
	jmp 1b

	.data
word_buffer:
	.space 255

/*
	Convert a word to a number.
	( addr length -- n rem )
	Rem is the number of characters that could not be converted.
	If non-zero, then the word was not entirely numerical.
	Calling _NUMBER directly leaves n in %rax and rem in %rcx.
	It expects addr in %rdi and length in %rcx.
*/
	defcode "NUMBER", 6, , NUMBER
	pop %rcx
	pop %rdi
	call _NUMBER
	push %rax
	push %rcx
	NEXT
_NUMBER:
	xor %eax, %eax
	xor %r8d, %r8d
	xor %ebx, %ebx

	test %rcx, %rcx
	jz 5f

	movq var_BASE(%rip), %rdx

	movb (%rdi), %bl
	inc %rdi
	cmpb $'-', %bl
	jnz 2f
	mov %ebx, %r8d
	dec %rcx
	jnz 1f
	movl $1, %ecx
	ret
1:
	imulq %rdx, %rax
	movb (%rdi), %bl
	inc %rdi
2:
	subb $'0', %bl
	jb 4f
	cmp $10, %bl
	jb 3f
	subb $17, %bl /* 'A' - '0' = 17 */
	jb 4f
	addb $10, %bl
3:
	cmp %dl, %bl
	jge 4f

	add %rbx, %rax
	dec %rcx
	jnz 1b
4:
	test %r8d, %r8d
	jz 5f
	neg %rax
5:
	ret

/*
	Look up a word in the dictionary.
	( addr length -- word )
*/
	defcode "FIND", 4, , FIND
	pop %rcx
	pop %rdi
	call _FIND
	push %rax
	NEXT
_FIND:
	mov %rsi, %r13
	movq var_LATEST(%rip), %rdx
1:
	test %rdx, %rdx
	je 3f

	xor %eax, %eax
	movb 8(%rdx), %al
	andb $F_HIDDEN, %al
	test %al, %al
	jne 2f
	movb 9(%rdx), %al
	cmpb %cl, %al
	jne 2f

	mov %rcx, %r8
	mov %rdi, %r9
	lea 10(%rdx), %rsi
	repe cmpsb
	mov %r9, %rdi
	mov %r8, %rcx
	jne 2f

	mov %r13, %rsi
	mov %rdx, %rax
	ret
2:
	mov (%rdx), %rdx
	jmp 1b
3:
	mov %r13, %rsi
	xor %eax, %eax
	ret

/*
	Load the address of the codeword
	( word -- codeword )
*/
	defcode ">CFA", 4, , TCFA
	pop %rdi
	call _TCFA
	push %rdi
	NEXT
_TCFA:
	xor %eax, %eax
	add $9, %rdi
	movb (%rdi), %al
	inc %rdi
	add %rax, %rdi
	addq $7, %rdi
	andq $~7, %rdi
	ret

/*
	Load the address of the first dataword
	( word -- data-addr )
*/

	defword ">DFA", 4, , TDFA
	.quad TCFA
	.quad INCR8
	.quad EXIT

/*
	Create a new dictionary entry.
	Does NOT create a codeword or data.
*/
	defcode "CREATE", 6, , CREATE
	pop %rcx
	pop %rbx

	movq var_HERE(%rip), %rdi
	movq var_LATEST(%rip), %rax
	stosq

	xor %eax, %eax
	stosb
	mov %cl, %al
	stosb
	mov %rsi, %r8
	mov %rbx, %rsi
	rep movsb
	mov %r8, %rsi
	addq $7, %rdi
	andq $~7, %rdi

	movq var_HERE(%rip), %rax
	movq %rax, var_LATEST(%rip)
	movq %rdi, var_HERE(%rip)
	NEXT

/*
	Append the top-of-stack to the data segment.
	This can only append 8-bytes at a time.
*/
	defcode ",", 1, , COMMA
	pop %rax
	call _COMMA
	NEXT
_COMMA:
	movq var_HERE(%rip), %rdi
	stosq
	movq %rdi, var_HERE(%rip)
	ret

/* [ and ] can be used together to interpret some words inside of a definition, eg. to calculate a value that will be compiled. */

/* Enter interpret mode. */
	defcode "[", 1, F_IMMED, LBRAC
	xor %eax, %eax
	movq %rax, var_STATE(%rip)
	NEXT

/* Enter compile mode. */
	defcode "]", 1, , RBRAC
	movq $1, var_STATE(%rip)
	NEXT

/* Define a word */
	defword ":", 1, , COLON
	.quad WORD
	.quad CREATE
	.quad LIT, DOCOL, COMMA
	.quad LATEST, FETCH, HIDDEN
	.quad RBRAC
	.quad EXIT

/* End a definition */
	defword ";", 1, F_IMMED, SEMICOLON
	.quad LIT, EXIT, COMMA
	.quad LATEST, FETCH, HIDDEN
	.quad LBRAC
	.quad EXIT

/* Mark the latest word as immediate */
	defcode "IMMEDIATE", 9, F_IMMED, IMMEDIATE
	movq var_LATEST(%rip), %rdi
	addq $8, %rdi
	xorb $F_IMMED, (%rdi)
	NEXT

/* Mark a word as hidden */
	defcode "HIDDEN", 6, , HIDDEN
	pop %rdi
	addq $8, %rdi
	xorb $F_HIDDEN, (%rdi)
	NEXT

/* Hide a word */
	defword "HIDE", 4, , HIDE
	.quad WORD
	.quad FIND
	.quad HIDDEN
	.quad EXIT

/* ' is equivalent to LIT. It's meant to work in immediate mode, but this version doesn't. */
	defcode "'", 1, , TICK
	lodsq
	push %rax
	NEXT

/* Branch instructions */
	defcode "BRANCH", 6, , BRANCH
	addq (%rsi), %rsi
	NEXT

	defcode "0BRANCH", 7, , ZBRANCH
	pop %rax
	test %rax, %rax
	jz code_BRANCH
	lodsq
	NEXT


/* Following LITSTRING should be an 8-byte length and the string data (padding to the next 8-byte boundary). */
	defcode "LITSTRING", 9, , LITSTRING
	lodsq
	push %rsi
	push %rax
	addq %rax, %rsi
	addq $7, %rsi
	andq $~7, %rsi
	NEXT

/* Print a string to standard output. */
	defcode "TELL", 4, , TELL
	movl $1, %edi
	pop %rdx
	mov %rsi, %r13
	pop %rsi
	movl $1, %eax
	syscall
	mov %r13, %rsi
	NEXT

/* Interpret words one-by-one. */
	defword "QUIT", 4, , QUIT
	.quad RZ, RSPSTORE
	.quad INTERPRET
	.quad BRANCH, -16

/* Interpret a single word. */
	defcode "INTERPRET", 9, , INTERPRET
	call _WORD
	xor %eax, %eax
	xor %r12, %r12
	call _FIND
	test %rax, %rax
	jz 1f

	mov %rax, %rdi
	movb 8(%rdi), %r8b
	call _TCFA
	andb $F_IMMED, %r8b
	mov %rdi, %rax
	jnz 4f

	jmp 2f
1:
	inc %r12
	call _NUMBER
	test %rcx, %rcx
	jnz 6f
	mov %rax, %rbx
	leaq LIT(%rip), %rax
2:
	movq var_STATE(%rip), %rdx
	test %rdx, %rdx
	jz 4f

	call _COMMA
	test %r12, %r12
	jz 3f
	mov %rbx, %rax
	call _COMMA
3:
	NEXT
4:
	test %r12, %r12
	jnz 5f
	jmp *(%rax)
5:
	push %rbx
	NEXT
6:
	mov %rsi, %r13
	movq $2, %rdi
	leaq errmsg(%rip), %rsi
	leaq errmsgend(%rip), %rdx
	subq %rsi, %rdx
	movq $1, %rax
	syscall

	movq currkey(%rip), %rsi
	movq %rsi, %rdx
	leaq buffer(%rip), %rax
	subq %rax, %rdx
	cmp $40, %rdx
	jle 7f
	mov $40, %rdx
7:
	subq %rdx, %rsi
	mov $1, %eax
	syscall

	leaq errmsgnl(%rip), %rsi
	mov $1, %edx
	mov $1, %eax
	syscall

	mov %r13, %rsi
	NEXT

	.section .rodata
errmsg: .ascii "PARSE ERROR: "
errmsgend:
errmsgnl: .ascii "\n"

/* Load the first byte of the following word */
	defcode "CHAR", 4, , CHAR
	call _WORD
	xor %eax, %eax
	movb (%rdi), %al
	push %rax
	NEXT

/* ( addr -- ) Continues execution at addr */
	defcode "EXECUTE", 7, , EXECUTE
	pop %rax
	jmp *(%eax)

/*
	Load the address of dynamically linked symbol "SYM"
	Calls a helper written in C.
*/
	defcode "SYM", 3, , SYM
	call _WORD
	movq %rsi, %r13

	mov $0xf, %r14
	and %rsp, %r14
	sub %r14, %rsp

	call real_SYM@PLT

	addq %r14, %rsp

	push %rax
	movq %r13, %rsi
	NEXT

/*
	( arg_n arg_n-1 ... arg1 symbol_address n -- result ) / ( symbol_address 0 -- result )
	Call an external funtion. Load the address with SYM first.
	Up to six integer / pointer arguments are supported.
	One integer result is returned.
	For functions that are not expected to return a value, the result is unspecified. Just DROP it.
*/
	defcode "CALL", 4, , CALL
	movq %rsi, %r13
	popq %r11
	popq %rax

	test %r11d, %r11d
	jz 1f
	pop %rdi
	dec %r11d

	test %r11d, %r11d
	jz 1f
	pop %rsi
	dec %r11d

	test %r11d, %r11d
	jz 1f
	pop %rdx
	dec %r11d

	test %r11d, %r11d
	jz 1f
	pop %rcx
	dec %r11d

	test %r11d, %r11d
	jz 1f
	pop %r8
	dec %r11d

	test %r11, %r11
	jz 1f
	pop %r9

1:

	mov $0xf, %r14
	and %rsp, %r14
	sub %r14, %rsp

	call *%rax

	addq %r14, %rsp

	pushq %rax
	movq %r13, %rsi
	NEXT

/*
	Cause another text file to be inserted into the input stream.
	eg. Z" program.forth" INCLUDE
	Requires a zero terminated string argument.
*/
	defcode "INCLUDE", 7, , INCLUDE
	pop %rdi
	movq %rsi, %r13
	mov $0xf, %r14
	and %rsp, %r14
	sub %r14, %rsp

	call real_INCLUDE@PLT

	addq %r14, %rsp

	push %rax
	movq %r13, %rsi
	NEXT

/*
	Variant of INCLUDE that reads a quote-terminated string from the input stream.
*/
	defcode "INCLUDE\"", 8, , INCLUDEQ
	movq %rsi, %r13
	mov $0xf, %r14
	and %rsp, %r14
	sub %r14, %rsp

	call real_INCLUDEQ@PLT

	addq %r14, %rsp
	push %rax
	movq %r13, %rsi
	NEXT

/* Easy system calls */
	defcode "SYSCALL3", 8, , SYSCALL3
	mov %rsi, %r11
	pop %rax
	pop %rdi
	pop %rsi
	pop %rdx
	push %r11
	syscall
	pop %rsi
	push %rax
	NEXT

	defcode "SYSCALL2", 8, , SYSCALL2
	mov %rsi, %r11
	pop %rax
	pop %rdi
	pop %rsi
	push %r11
	syscall
	pop %rsi
	push %rax
	NEXT

	defcode "SYSCALL1", 8, , SYSCALL1
	pop %rax
	pop %rdi
	syscall
	push %rax
	NEXT

	defcode "SYSCALL0", 8, , SYSCALL0
	pop %rax
	syscall
	push %rax
	NEXT

	.set INITIAL_DATA_SEGMENT_SIZE, 0x40000

	.set RETURN_STACK_SIZE, 8192

	.bss
	.align 4096
rstack_base:
	.space RETURN_STACK_SIZE
rstack_top:
