#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <stdio.h>
#include <dlfcn.h>
#include <string.h>
#include <fcntl.h>

_Noreturn void begin(int argc, char **argv);
char real_KEY(void);

void *data_start;
size_t data_size;

#define INITIAL_DATA_SIZE 0x40000

#define BUFFER_SIZE 4096

char buffer[BUFFER_SIZE];
char *bufftop = buffer;
char *currkey = buffer;

#define MAX_INCLUDES 32

static int source_count;
static char **source_names;
static int source_fd;

struct include_file {
	struct include_file *prev_file;
	char *name;
	int prev_fd;
	char prev_buffer[BUFFER_SIZE];
	char *prev_bufftop;
	char *prev_currkey;
};

static struct include_file *curr_include = NULL;

int real_INCLUDE(const char *name) {
	struct include_file *file = malloc(sizeof(struct include_file));
	if(!file) {
		perror(NULL);
		return 0;
	}
	file->prev_file = curr_include;
	file->name = strdup(name);
	if(!file->name) {
		perror(NULL);
		free(file);
		return 0;
	}
	file->prev_fd = source_fd;
	source_fd = open(name, O_RDONLY);
	if(source_fd == -1) {
		perror(name);
		source_fd = file->prev_fd;
		free(file->name);
		free(file);
		return 0;
	}
	memcpy(file->prev_buffer, buffer, sizeof buffer);
	file->prev_bufftop = bufftop;
	file->prev_currkey = currkey;
	bufftop = buffer;
	currkey = buffer;
	curr_include = file;
	return 1;
}

int real_INCLUDEQ(void) {
	char path[256];
	int len = 0;
	while(1) {
		char c = real_KEY();
		if(c == '\\') {
			c = real_KEY();
		} if(c == '"') {
			break;
		}

		if(len < 255) {
			path[len++] = c;
		}
	}
	path[len] = 0;
	return real_INCLUDE(path);
}

static _Bool next_source_file(void) {
	if(curr_include) {
		memcpy(buffer, curr_include->prev_buffer, sizeof buffer);
		bufftop = curr_include->prev_bufftop;
		currkey = curr_include->prev_currkey;
		free(curr_include->name);
		source_fd = curr_include->prev_fd;
		struct include_file *prev_file = curr_include->prev_file;
		free(curr_include);
		curr_include = prev_file;
		return 1;
	} else {
		source_names++;
		source_count--;

		if(source_count) {
			source_fd = open(*source_names, O_RDONLY);
			if(source_fd == -1) {
				perror(*source_names);
				exit(1);
			}
		} else {
			source_fd = 0;
		}
		return 0;
	}
}

char real_KEY(void) {
begin:
	if(currkey < bufftop) {
		return *currkey++;
	}

	ssize_t length;
	while((length = read(source_fd, buffer,  BUFFER_SIZE)) == 0) {
		if(!source_fd) {
			exit(0);
		}
		if(next_source_file()) {
			goto begin;
		}
	}

	bufftop = buffer + length;
	currkey = buffer + 1;

	return *buffer;
}

static void *dl_self;

void *real_SYM(const char *name) {
	return dlsym(dl_self, name);
}

static void free_data(void) {
	free(data_start);
}

int main(int argc, char **argv) {
	int arg_start = 0;
	for(int i = 1; i < argc; i++) {
		if(!strcmp(argv[i], "--")) {
			arg_start = i;
			break;
		}
	}

	if(arg_start != 0) {
		if(arg_start > 1) {
			source_names = &argv[1];
			source_count = arg_start - 1;
			source_fd = open(*source_names, O_RDONLY);
			if(source_fd == -1) {
				perror(*source_names);
				exit(1);
			}
		}
		argv[arg_start] = argv[0];
	}

	dl_self = dlopen(NULL, RTLD_LAZY);
	data_start = malloc(INITIAL_DATA_SIZE);
	if(!data_start) {
		perror(NULL);
		exit(1);
	}
	data_size = INITIAL_DATA_SIZE;

	atexit(free_data);

	begin(argc - arg_start, &argv[arg_start]);
}
