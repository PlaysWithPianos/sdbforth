.PHONY: all clean cleanobjs cleandeps cleaner

.PRECIOUS: %/ %.d

all:

all_objs := 

%.d: ;

include $(wildcard *.mk)

all_deps := $(all_objs:%.o=%.d)
include $(wildcard $(all_deps))

%/:
	mkdir -p $@

clean: cleanobjs

cleanobjs:
ifneq ($(all_objs),)
	$(RM) $(all_objs)
endif

cleandeps:
ifneq ($(all_deps),)
	$(RM) $(all_deps)
endif

cleaner:
	$(RM) -r obj/ bin/
